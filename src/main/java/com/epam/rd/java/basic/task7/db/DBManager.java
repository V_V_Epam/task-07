package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	//private static final String USER = "root";
	//private static final String PASSWORD = "root";
	//private static final String URL = "jdbc:mysql://localhost:3306/test2db" +
	//		"?sslMode=DISABLED&serverTimezone=UTC&user=" +
	//		USER + "&password=" + PASSWORD;
	private static String URL;

	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM USERS ORDER BY ID";
	private static final String SQL_FIND_USER = "SELECT * FROM USERS WHERE LOGIN = ?";
	private static final String SQL_INSERT_USER = "INSERT INTO USERS (login) VALUES (?)";
	private static final String SQL_DELETE_USER = "DELETE FROM USERS WHERE ID = ?";

	private static final String SQL_INSERT_TEAM = "INSERT INTO TEAMS (name) VALUES (?)";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM TEAMS ORDER BY NAME";
	private static final String SQL_DELETE_TEAM = "DELETE FROM TEAMS WHERE NAME = ?";

	private static final String SQL_DELETE_USER_TEAM_BY_TEAM_ID = "DELETE FROM USERS_TEAMS WHERE TEAM_ID = ?";
	private static final String SQL_DELETE_USER_TEAM_BY_USER_ID = "DELETE FROM USERS_TEAMS WHERE USER_ID = ?";
	private static final String SQL_UPDATE_TEAM = "UPDATE TEAMS SET NAME = ? WHERE ID = ?";
	private static final String SQL_FIND_TEAM = "SELECT * FROM TEAMS WHERE NAME = ?";

	private static final String SQL_INSERT_USER_TEAM = "INSERT INTO USERS_TEAMS (USER_ID, TEAM_ID) VALUES (?, ?)";
	private static final String SQL_FIND_USER_TEAMS = "SELECT * FROM TEAMS WHERE id in (SELECT team_id FROM USERS_TEAMS WHERE USER_ID IN (SELECT ID FROM USERS WHERE LOGIN = ?))";

	private static DBManager instance;

	//read url data from properties file
	private String readProperty() {
		String url = null;
		FileInputStream fis;
		Properties property = new Properties();
		try {
			fis = new FileInputStream("app.properties");
			property.load(fis);
			url = property.getProperty("connection.url");
			System.out.println(url);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;
	}

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		URL = readProperty();
	}

	private void closeAny(AutoCloseable element) {
		try {
			if (element != null) {
				element.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Task1
	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS))
		{
			while (rs.next()) {
				int columnIndex = 1;
				int id = rs.getInt(columnIndex++);
				String login = rs.getString(columnIndex++);
				User user = User.createUser(login);
				user.setId(id);
				users.add(user);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception findAllUsers", throwables);
		}
		return users;
	}

	//For insertUser Task1
	//For setTeamsForUser Task3
	//for getUserTeams Task3
	public User getUser(String login) throws DBException {
		User user = null;
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_FIND_USER))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, login);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				columnIndex = 1;
				int id = rs.getInt(columnIndex++);
				String log = rs.getString(columnIndex++);
				user = new User(log);
				user.setId(id);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception getUser", throwables);
		} finally {
			closeAny(rs);
		}
		return user;
	}

	//Task1
	public boolean insertUser(User user) throws DBException {
		if (getUser(user.getLogin()) != null) {
			return false;
		}
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_USER,
					 Statement.RETURN_GENERATED_KEYS))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, user.getLogin());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					columnIndex = 1;
					user.setId(rs.getInt(columnIndex++));
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception insertUser", throwables);
		} finally {
			closeAny(rs);
		}
		return true;
	}

	//Task6
	public boolean deleteUsers(User... users) throws DBException {
		boolean isDelete = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		//deleteUsersTeams(User user.getId(), Connection con)
		try {
			con = DriverManager.getConnection(URL);
			pstmt = con.prepareStatement(SQL_DELETE_USER);
			for (User user : users) {
				con.setAutoCommit(false);
				int columnIndex = 1;
				pstmt.setInt(columnIndex++, user.getId());
				deleteUsersTeams(user, con);
				if (pstmt.executeUpdate() > 0) {
					isDelete = true;
				}
				con.commit();
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("Exception deleteUsers", throwables);
			}
			throw new DBException("Exception deleteUsers", throwables);
		} finally {
			closeAny(pstmt);
			closeAny(con);
		}
		return isDelete;
	}

	//for setTeamsForUser Task3
	public Team getTeam(String name) throws DBException {
		Team team = null;
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_FIND_TEAM))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, name);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				columnIndex = 1;
				int id = rs.getInt(columnIndex++);
				String nameTemp = rs.getString(columnIndex++);
				team = new Team();
				team.setId(id);
				team.setName(nameTemp);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception getTeam", throwables);
		} finally {
			closeAny(rs);
		}
		return team;
	}

	//Task2
	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS))
		{
			while (rs.next()) {
				int columnIndex = 1;
				int id = rs.getInt(columnIndex++);
				String name = rs.getString(columnIndex++);
				Team team = Team.createTeam(name);
				team.setId(id);
				teams.add(team);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception findAllTeams", throwables);
		}
		return teams;
	}

	//Task2
	public boolean insertTeam(Team team) throws DBException {
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_TEAM,
					 Statement.RETURN_GENERATED_KEYS))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, team.getName());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					columnIndex = 1;
					team.setId(rs.getInt(columnIndex++));
				}
			} else {
				return false;
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception insertTeam", throwables);
		} finally {
			closeAny(rs);
		}
		return true;
	}

	//Task3
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		//несуществующему пользователю в БД наззначить группы нельзя
		if (getUser(user.getLogin()) == null) {
			return false;
		}
		//назначить можно только существующие группы
		List<Team> teamsExist = new ArrayList<>();
		for (Team team : teams) {
			if (getTeam(team.getName()) != null) {
				teamsExist.add(team);
			}
		}
		if (teamsExist.size() == 0) {
			return false;
		}
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = DriverManager.getConnection(URL);
			pstmt = con.prepareStatement(SQL_INSERT_USER_TEAM);
			con.setAutoCommit(false);
			for (Team team : teamsExist) {
				int columnIndex = 1;
				pstmt.setInt(columnIndex++, user.getId());
				pstmt.setInt(columnIndex++, team.getId());
				pstmt.executeUpdate();
			}
			con.commit();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("Exception setTeamsForUser", throwables);
			}
			throw new DBException("Exception setTeamsForUser", throwables);
		} finally {
			closeAny(pstmt);
			closeAny(con);
		}
		return true;
	}

	//Task3
	public List<Team> getUserTeams(User user) throws DBException {
		if (getUser(user.getLogin()) == null) {
			return null;
		}
		List<Team> teams = new ArrayList<>();
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_FIND_USER_TEAMS))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, user.getLogin());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				columnIndex = 1;
				int id = rs.getInt(columnIndex++);
				String name = rs.getString(columnIndex++);
				Team team = Team.createTeam(name);
				team.setId(id);
				teams.add(team);
			}
		} catch (SQLException throwables) {
			closeAny(rs);
			throwables.printStackTrace();
			throw new DBException("Exception getUserTeams", throwables);
		}
		return teams;
	}

	//for deleteTeam Task4
	private boolean deleteUsersTeams(Team team, Connection con) throws SQLException {
		PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_USER_TEAM_BY_TEAM_ID);
		int columnIndex = 1;
		pstmt.setInt(columnIndex++, team.getId());
		if (pstmt.executeUpdate() > 0) {
			return true;
		}
		return false;
	}

	//for deleteUsers Task6
	private boolean deleteUsersTeams(User user, Connection con) throws SQLException {
		PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_USER_TEAM_BY_USER_ID);
		int columnIndex = 1;
		pstmt.setInt(columnIndex++, user.getId());
		if (pstmt.executeUpdate() > 0) {
			return true;
		}
		return false;
	}

	//Task4
	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = DriverManager.getConnection(URL);
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			con.setAutoCommit(false);
			int columnIndex = 1;
			pstmt.setString(columnIndex++, team.getName());

			deleteUsersTeams(team, con);

			if (pstmt.executeUpdate() > 0) {
				con.commit();
				return true;
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("Exception deleteTeam", throwables);
			}
			throw new DBException("Exception deleteTeam", throwables);
		} finally {
			if (pstmt != null) {
				closeAny(pstmt);
			}
			if (con != null) {
				closeAny(con);
			}
		}
		return false;
	}

	//Task5
	public boolean updateTeam(Team team) throws DBException {
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_TEAM))
		{
			int columnIndex = 1;
			pstmt.setString(columnIndex++, team.getName());
			pstmt.setInt(columnIndex++, team.getId());
			if (pstmt.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Exception updateTeam", throwables);
		} finally {
			closeAny(rs);
		}
		return false;
	}

}
